﻿/*
Savagedlight
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    /// <summary>
    /// Tracks available implementations of IStreamDataParser&lt;T&gt;
    /// </summary>
    internal class StreamDataParserRegistry
    {
        /// <summary>
        /// Registered stream data parsers
        /// </summary>
        private Dictionary<Type, StreamDataParser> streamDataParsers = new Dictionary<Type, StreamDataParser>();
        private SmartReaderWriterLock streamDataParsersLock = new SmartReaderWriterLock();
        /// <summary>
        /// Which assemblies have already been registered.
        /// </summary>
        private HashSet<Assembly> registeredAssemblies = new HashSet<Assembly>();

        internal static StreamDataParserRegistry Instance { get; private set; }
        static StreamDataParserRegistry()
        {
            Instance = new StreamDataParserRegistry();
        }

        private StreamDataParserRegistry()
        {
            this.RegisterStreamDataParsers(Assembly.GetAssembly(typeof(StreamData)));
        }

        /// <summary>
        /// Retrieves the parser assigned to the speicifed t. If none is availabe, a parser implementing IStreamDataParser&lt;object&gt; will be returned.
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        internal bool TryGetParser(Type dataType, out StreamDataParser parser)
        {
            using (this.streamDataParsersLock.ReadLock())
            {
                return this.streamDataParsers.TryGetValue(dataType, out parser);
            }            
        }

        /// <summary>
        /// Find and utilize all StreamDataParsers within assembly
        /// </summary>
        /// <param name="assembly"></param>
        internal void RegisterStreamDataParsers(Assembly assembly)
        {            
            if (this.registeredAssemblies.Contains(assembly))
            {
                return;
            }
            this.registeredAssemblies.Add(assembly);

            Type interfaceType = typeof(IStreamDataParser<>);            
            var candidates = from t in assembly.GetTypes()
                             // Only consider classes
                             where t.IsClass
                             // All interfaces which implement IStreamDataParser<>
                             let interfaces = t.GetInterfaces().Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType).ToArray()
                             // Only types which have at least one IStreamDataParser<>
                             where interfaces.Length != 0
                             select new KeyValuePair<Type, Type[]>(t, interfaces);

            foreach (KeyValuePair<Type, Type[]> kvp in candidates)
            {
                RegisterStreamDataParser(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Register a specific StreamDataParser.
        /// </summary>
        /// <param name="parser"></param>
        private void RegisterStreamDataParser(Type parser, Type[] interfaces)
        {
            object instance = Activator.CreateInstance(parser);
            // Iterate all the parsers.
            using (this.streamDataParsersLock.WriteLock())
            {
                foreach (Type type in interfaces)
                {
                    StreamDataParser sdp = new StreamDataParser(instance, type);
                    this.streamDataParsers[sdp.DataType] = sdp;
                }
            }
        }
    }
}
