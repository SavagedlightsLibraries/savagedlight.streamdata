﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    internal static class DeserializeHelper
    {
        internal static void DeserializeProperty(object obj, SuperStream stream, StreamDataPropertyMetaData pMeta)
        {
            object value;
            try
            {
                GetParserData(stream, pMeta, out value);
            }
            catch (Exception ex)
            {
                throw new ParserErrorException(pMeta, inner: ex);
            }
            pMeta.PropertyInfo.SafeSetValue(obj, value, null);
        }

        internal static void DeserializeCollection(object obj, SuperStream stream, StreamDataPropertyMetaData pMeta)
        {
            int entries = pMeta.ReadContentLength(stream);
            object arr2;

            // Hack for byte arrays
            if (pMeta.StreamType == typeof(byte))
            {
                arr2 = new ArrayList(stream.ReadBytes((uint)entries)).ToArray(typeof(byte));
            }
            else
            {
                ArrayList arr = ReadCollectionElements(stream, pMeta, entries);
                arr2 = arr.ToArray(pMeta.DataType);
            }

            if (pMeta.IsArray)
            {
                pMeta.PropertyInfo.SafeSetValue(obj, arr2, null);
            }
            else if (pMeta.IsList)
            {
                object list = Activator.CreateInstance(
                    typeof(List<>).MakeGenericType(pMeta.DataType),
                    arr2);
                pMeta.PropertyInfo.SafeSetValue(obj, list, null);
            }
        }

        private static ArrayList ReadCollectionElements(SuperStream stream, StreamDataPropertyMetaData pMeta, int entries)
        {
            ArrayList arr = new ArrayList((int)Math.Min(int.MaxValue, entries));
            object value;
            for (int i = 0; i < entries; i++)
            {
                try
                {
                    GetParserData(stream, pMeta, out value);
                }
                catch (Exception ex)
                {
                    throw new ParserErrorException(pMeta, "Collection element number " + i.ToString(), ex);
                }
                arr.Add(value);
            }
            return arr;
        }

        /// <summary>
        /// Retrieve data as described by description
        /// </summary>
        /// <param name="stream">Stream to get <paramref name="value"/> from</param>
        /// <param name="pMeta">How to retrieve value</param>
        /// <param name="value">Populated with the read value</param>
        private static void GetParserData(SuperStream stream, StreamDataPropertyMetaData pMeta, out object value)
        {
            StreamDataParser parser;
            object tmpVal;
            if (StreamDataParserRegistry.Instance.TryGetParser(pMeta.StreamType, out parser))
            {
                // Populated by the deserializer            
                parser.Deserialize(stream, pMeta.ParserInfo, out tmpVal);
                value = CastFromStreamToObjectType(pMeta, tmpVal);
                return;
            }

            // Implement support for nested types
            if (!typeof(IStreamDataAware).IsAssignableFrom(pMeta.StreamType))
            {
                throw new UnsupportedTypeException(pMeta);
            }

            tmpVal = StreamData.Deserialize(pMeta.StreamType, stream);
            value = CastFromStreamToObjectType(pMeta, tmpVal);
            return;
        }

        private static object CastFromStreamToObjectType(StreamDataPropertyMetaData pMeta, object tmpVal)
        {
            object value;
            // Cast value, if necessary.
            if (pMeta.StreamType != pMeta.DataType)
            {
                // Is it an enum?
                if (pMeta.DataType.IsEnum)
                {
                    value = Enum.ToObject(pMeta.DataType, tmpVal);
                }
                else
                {
                    value = Convert.ChangeType(tmpVal, pMeta.DataType);
                }
            }
            else
            {
                value = tmpVal;
            }
            return value;
        }
    }
}
