﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    /// <summary>
    /// Tracks known types which implement IStreamDataAware.
    /// </summary>
    internal class StreamDataAwareRegistry
    {
        private HashSet<Assembly> registeredAssemblies = new HashSet<Assembly>();
        private Dictionary<Type, StreamDataAwareMetaData> knownTypes = new Dictionary<Type, StreamDataAwareMetaData>();
        private SmartReaderWriterLock knownTypesLock = new SmartReaderWriterLock();

        internal static StreamDataAwareRegistry Instance { get; private set; }
        static StreamDataAwareRegistry()
        {
            Instance = new StreamDataAwareRegistry();
        }

        private StreamDataAwareRegistry()
        {
            this.RegisterStreamDataAwareTypes(this.GetType().Assembly);
        }

        internal void RegisterStreamDataAwareTypes(Assembly assembly)
        {
            if (this.registeredAssemblies.Contains(assembly))
            {
                return;
            }
            this.registeredAssemblies.Add(assembly);

            Type interfaceType = typeof(IStreamDataAware);

            IEnumerable<Type> candidates = from t in assembly.GetTypes()
                             where interfaceType.IsAssignableFrom(t)
                             select t;
            foreach (Type t in candidates)
            {
                this.CreateInfo(t);
            }
        }

        private void CreateInfo(Type t)
        {
            using (knownTypesLock.WriteLock())
            {
                if (knownTypes.ContainsKey(t))
                {
                    return;
                }
                knownTypes.Add(t, new StreamDataAwareMetaData(t));
            }
        }

        internal StreamDataAwareMetaData GetInfo(Type t)
        {
            if (!this.knownTypes.ContainsKey(t))
            {
                this.CreateInfo(t);
            }
            using (this.knownTypesLock.ReadLock())
            {
                return this.knownTypes[t];
            }
        }
    }
}
