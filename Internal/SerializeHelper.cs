﻿/*
Savagedlight
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    internal static class SerializeHelper
    {
        internal static void SerializeCollection(object obj, SuperStream stream, StreamDataPropertyMetaData pMeta, object value)
        {
            // Find actual length of collection.
            ArrayList list = (ArrayList)Activator.CreateInstance(typeof(ArrayList), value);
            int length = list.Count;
            if (!pMeta.IsLengthInRange(length))
            {
                throw new CollectionLengthOverflowException(pMeta);
            }
            pMeta.WriteContentLength(stream, length);

            // Hack for byte arrays
            if (pMeta.StreamType == typeof(byte))
            {
                stream.WriteBytes((byte[])list.ToArray(typeof(byte)));
                return;
            }

            SerializeCollectionEntry(stream, pMeta, list);
        }        

        internal static void SerializeProperty(object obj, SuperStream stream, StreamDataPropertyMetaData pMeta, object value)
        {
            try
            {
                WriteParserData(stream, pMeta, value);
            }
            catch (Exception ex)
            {
                throw new ParserErrorException(pMeta, inner: ex);
            }
        }

        private static void SerializeCollectionEntry(SuperStream stream, StreamDataPropertyMetaData pMeta, ArrayList list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                object entry = list[i];
                try
                {
                    WriteParserData(stream, pMeta, entry);
                }
                catch (Exception ex)
                {
                    throw new ParserErrorException(pMeta, "Collection element number " + i.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Write data as described by description
        /// </summary>
        /// <param name="stream">Stream to write <paramref name="value"/> to</param>
        /// <param name="pMeta">How to serialize object</param>
        /// <param name="value">Object to serialize</param>
        private static void WriteParserData(SuperStream stream, StreamDataPropertyMetaData pMeta, object value)
        {
            object tmpVal;
            // Cast value, if necessary.
            if (pMeta.StreamType != pMeta.DataType)
            {
                tmpVal = Convert.ChangeType(value, pMeta.StreamType);
            }
            else
            {
                tmpVal = value;
            }

            StreamDataParser parser;
            if (StreamDataParserRegistry.Instance.TryGetParser(pMeta.StreamType, out parser))
            {
                parser.Serialize(stream, pMeta.ParserInfo, tmpVal);
                return;
            }

            // Implement support for nested types
            if (!typeof(IStreamDataAware).IsAssignableFrom(pMeta.StreamType))
            {
                throw new UnsupportedTypeException(pMeta);
            }
            StreamData.Serialize((IStreamDataAware)tmpVal, stream);
        }
    }
}
