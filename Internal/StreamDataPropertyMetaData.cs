﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    internal class StreamDataPropertyMetaData
    {
        /// <summary>
        /// The property being described
        /// </summary>
        internal PropertyInfo PropertyInfo { get; private set; }

        /// <summary>
        /// Whether the propertys data t is an array
        /// </summary>
        internal bool IsArray { get { return this.PropertyInfo.PropertyType.IsArray; } }

        /// <summary>
        /// Whether the propertys t is a list
        /// </summary>
        internal bool IsList { get; private set; }

        /// <summary>
        /// Whether the propertys t is a collection
        /// </summary>
        internal bool IsCollection { get { return this.IsArray || this.IsList; } }

        /// <summary>
        /// Propertys t, or element t if it's a collection.
        /// </summary>
        internal Type DataType { get; private set; }

        /// <summary>
        /// Attributes attached to the property.
        /// </summary>
        internal Attribute[] Attributes { get; set; }
        /// <summary>
        /// Which t is read from or written to stream
        /// </summary>
        internal Type StreamType { get; private set; }

        internal StreamDataParserInfo ParserInfo { get; private set; }

        private SDCollectionLengthAttribute CollectionLength { get; set; }

        internal StreamDataPropertyMetaData(PropertyInfo pi, StreamDataAttribute attr)
        {
            this.PropertyInfo = pi;
            this.Attributes = this.PropertyInfo.GetCustomAttributes(true).Cast<Attribute>().ToArray();
            this.CollectionLength = this.PropertyInfo.GetAttribute<SDCollectionLengthAttribute>(true);
            this.StreamType = attr.StreamType;
            
            // Check if it's a list
            this.IsList = this.PropertyInfo.PropertyType.IsGenericType
                && this.PropertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(List<>);

            this.ValidateStreamType();
            this.AssignDataTypes();
            #warning Todo: Verify that we can cast between StreamType and DataType.
            this.ValidateCollectionParameters();

            this.ParserInfo = new StreamDataParserInfo(this);
        }

        private void ValidateStreamType()
        {
            // Validate input
            if (this.StreamType != null && this.StreamType.IsArray)
            {
                string msg = String.Format("{0}->{1}: [StreamDataAttribute] specified ReadType=typeof({2}), but {2} is an array. Specify its member type {3} instead.",
                    this.PropertyInfo.DeclaringType.FullName,
                    this.PropertyInfo.Name,
                    this.StreamType.Name,
                    this.StreamType.MemberType);

                throw new StreamDataException(this, msg);
            }
        }
        private void ValidateCollectionParameters()
        {
            // Verify that collections have correct parameters
            if (this.IsCollection && this.CollectionLength == null)
            {
                string msg = String.Format(
                        "{0}->{1}: Collection must have a [SDCollectionLengthAttribute].",
                        this.PropertyInfo.DeclaringType.FullName,
                        this.PropertyInfo.Name);
                throw new StreamDataException(this, msg);
            }

            if (this.PropertyInfo.PropertyType.IsGenericType)
            {
                Type t1 = this.PropertyInfo.PropertyType.GetGenericTypeDefinition();
                Type t2 = typeof(IEnumerable<>);
                if (t2.IsAssignableFrom(t1))
                {
                    throw new UnsupportedCollectionException(this);
                }
            }
        }

        /// <summary>
        /// Assigns DataType and StreamType Properties
        /// </summary>
        private void AssignDataTypes()
        {
            // Figure out data type
            if (this.IsArray)
            {
                this.DataType = this.PropertyInfo.PropertyType.GetElementType();
            }
            else if (this.IsList)
            {
                this.DataType = this.PropertyInfo.PropertyType.GetGenericArguments()[0];
            }
            else
            {
                this.DataType = this.PropertyInfo.PropertyType;
            }

            // figure out stream data type
            if (this.StreamType == null)
            {
                if (this.DataType.IsEnum)
                {
                    this.StreamType = Enum.GetUnderlyingType(this.PropertyInfo.PropertyType);
                }
                else
                {
                    this.StreamType = this.DataType;
                }
            }
        }

        /// <summary>
        /// Retrieves content length from <paramref name="stream"/>
        /// </summary>
        /// <param name="stream">Stream to read from</param>
        /// <returns>Record length</returns>
        #warning TODO: Move this elsewhere
        internal int ReadContentLength(SuperStream stream)
        {
            if (this.CollectionLength.Entries != 0)
            {
                return this.CollectionLength.Entries;
            }

            switch (this.CollectionLength.Type)
            {
                case LengthType.Byte:
                    return (byte)stream.ReadByte();
                case LengthType.UInt16:
                    return stream.ReadUInt16();
                default:
                case LengthType.Int32:
                    return stream.ReadInt32();
            }
        }

        /// <summary>
        /// Retrieves a record length from <paramref name="stream"/>
        /// </summary>
        /// <param name="stream">Stream to read from</param>
        /// <param name="length">The length to write.</param>
        /// <exception cref="Exception">If <paramref name="length"/> is longer than the <seealso cref="LengthType"/> supports.</exception>
        #warning TODO: Move this elsewhere
        internal void WriteContentLength(SuperStream stream, int length)
        {
            if (this.CollectionLength.Entries != 0)
            {
                return;
            }

            switch (this.CollectionLength.Type)
            {
                case LengthType.Byte:
                    stream.WriteByte((byte)length);
                    break;
                case LengthType.UInt16:
                    stream.WriteUInt16((ushort)length);
                    break;
                default:
                case LengthType.Int32:
                    stream.WriteInt32(length);
                    break;
            }
        }

        /// <summary>
        /// Checks whether the specified length is in range of this propertys CollectionLength specification.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        #warning TODO: Move this elsewhere 
        internal bool IsLengthInRange(int length)
        {
            switch (this.CollectionLength.Type)
            {
                case LengthType.Byte:
                    if (length > byte.MaxValue)
                    {
                        return false;
                    }
                    break;
                case LengthType.UInt16:
                    if (length > ushort.MaxValue)
                    {
                        return false;
                    }
                    break;
            }
            return true;
        }
    }
}
