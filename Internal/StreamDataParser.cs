﻿/*
Savagedlight
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Serialization.Internal
{
    /// <summary>
    /// A wrapper for any class implementing <see cref="IStreamDataParser&lt;&gt;"/><br />
    /// </summary>
    internal class StreamDataParser
    {
        /// <summary>
        /// Parser instance. 
        /// </summary>
        /// <value>Is null, or an instanciated parser object.</value>
        private object Instance { get; set; }

        /// <summary>
        /// The data t this parser can (de)serialize.
        /// </summary>
        internal Type DataType { get; private set; }

        /// <summary>
        /// This <see cref="IStreamDataParser&lt;&gt;" />s Deserialize() method.
        /// </summary>
        private MethodInfo DeserializeMethodInfo { get; set; }

        /// <summary>
        /// This <see cref="IStreamDataParser&lt;&gt;" />s Serialize() method.
        /// </summary>
        private MethodInfo SerializeMethodInfo { get; set; }

        /// <summary>
        /// Creates a new instance describing a <see cref="IStreamDataParser&lt;&gt;" /> implementation.
        /// </summary>
        /// <param name="parserInstance">An instance of the class implementing <paramref name="interfaceType"/></param>
        /// <param name="interfaceType">The interface which will be described by this instance</param>
        internal StreamDataParser(object parserInstance, Type interfaceType)
        {
            this.Instance = parserInstance;
            this.DataType = interfaceType.GetGenericArguments()[0];

            this.SerializeMethodInfo = interfaceType.GetMethod("Serialize");
            this.DeserializeMethodInfo = interfaceType.GetMethod("Deserialize");
        }

        /// <summary>
        /// Serializes <paramref name="obj"/> according to <paramref name="description"/>
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="description"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal void Serialize(SuperStream stream, StreamDataParserInfo description, object obj)
        {
            if (obj == null) { throw new ArgumentNullException("obj"); }
            object[] parameters = new object[] { stream, description, obj };
            this.SerializeMethodInfo.Invoke(this.Instance, parameters);
        }

        /// <summary>
        /// Deserializes <paramref name="obj"/> according to <paramref name="description"/>
        /// </summary>
        /// <param name="description"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal void Deserialize(SuperStream stream, StreamDataParserInfo description, out object obj)
        {
            object[] parameters = new object[] { stream, description, null };
            this.DeserializeMethodInfo.Invoke(this.Instance, parameters);
            obj = parameters[2];
        }
    }
}
