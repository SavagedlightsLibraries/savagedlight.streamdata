﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight.Extensions;
using Savagedlight.Serialization.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Savagedlight.Serialization
{

    /// <summary>Provides an advanced API for (de)Serializing objects and streams.</summary>
    public static class StreamData
    {
        /// <summary>Register all <see cref="IStreamDataParser{T}">IStreamDataParser&lt;T&gt; Interface</see> implementations found within assembly, and pregenerate metadata for all
        /// <see cref="IStreamDataAware">IStreamDataAware Interface</see> implementations found within assembly.</summary>
        /// <param name="assembly"></param>
        /// <example>
        /// 	<code title="Example" description="" lang="CS">
        /// public static class MyContext 
        /// {
        ///     static MyContext() 
        ///     {
        ///         StreamData.RegisterAssembly(typeof(MyContext).Assembly);
        ///     }
        /// }</code>
        /// </example>
        public static void RegisterAssembly(Assembly assembly)
        {
            StreamDataParserRegistry.Instance.RegisterStreamDataParsers(assembly);
            StreamDataAwareRegistry.Instance.RegisterStreamDataAwareTypes(assembly);
        }

        /// <summary>
        /// Write a given object to stream
        /// </summary>
        /// <param name="obj">Object to write</param>
        /// <param name="stream">Stream to write to</param>
        /// <example>
        /// 	<code title="Example" description="" lang="C#">
        /// public void Save(FileInfo file)
        /// {
        ///     using (FileStream fileStream = file.OpenWrite()) 
        ///     {
        ///         using (SuperStream stream = new SuperStream(fileStream, Endianess.Little))
        ///         {
        ///             StreamData.Serialize(this, stream);
        ///         }    
        ///     }
        /// }</code>
        /// </example>
        public static void Serialize(IStreamDataAware obj, SuperStream stream)
        {
            StreamDataAwareMetaData tm = StreamDataAwareRegistry.Instance.GetInfo(obj.GetType());

            // Check if we should call a finalizer
            if (tm.IsFinalizer)
            {
                IStreamDataFinalizer fin = (IStreamDataFinalizer)obj;
                fin.OnSerialize();
            }

            // Parse spell arguments
            foreach (StreamDataPropertyMetaData pMeta in tm.Properties)
            {
                object value = pMeta.PropertyInfo.SafeGetValue(obj, null);
                if (pMeta.IsCollection)
                {
                    SerializeHelper.SerializeCollection(obj, stream, pMeta, value);
                }
                else
                {
                    SerializeHelper.SerializeProperty(obj, stream, pMeta, value);
                }
            }
        }

        /// <summary>Deserializes <see cref="stream"/> into a new instance <see cref="T"/>.</summary>
        /// <typeparam name="T">Type of object to deserialize</typeparam>
        /// <param name="stream">Stream to deserialize</param>
        /// <returns>A new object of t <typeparamref name="T"/>, populated with data from <paramref name="stream"/></returns>
        /// <example>
        /// 	<code title="Example" description="Creates an instance from a file" lang="C#">
        /// public class MySettings : IStreamDataAware
        /// {
        ///     // ...
        ///     
        ///     ///&lt;summary&gt;
        ///     /// Loads settings from file
        ///     ///&lt;/summary&gt;
        ///     public static MySettings Load(FileInfo file) 
        ///     {
        ///         using (FileStream fileStream = file.OpenRead()) 
        ///         {
        ///             using (SuperStream stream = new SuperStream(fileStream, Endianess.Little))
        ///             {
        ///                 return StreamData.Deserialize&lt;MySetting&gt;(stream);
        ///             }
        ///         }
        ///     }
        ///     
        ///     // ...
        /// }</code>
        /// </example>
        public static T Deserialize<T>(SuperStream stream)
            where T : IStreamDataAware
        {                        
            IStreamDataAware obj = Activator.CreateInstance<T>();
            Deserialize(ref obj, stream);
            return (T)obj;
        }

        /// <summary>Deserializes <paramref name="stream"/> into a new object of type <paramref name="t"/>.</summary>
        /// <param name="t">Type to deserialize</param>
        /// <param name="stream">Stream to read data from</param>
        /// <returns></returns>
        public static object Deserialize(Type t, SuperStream stream)
        {
            IStreamDataAware obj = (IStreamDataAware)Activator.CreateInstance(t);
            Deserialize(ref obj, stream);
            return obj;
        }

        /// <summary>Deserializes <paramref name="stream"/> into an existing <see cref="T"/> instance <paramref name="obj"/>.</summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="obj">Existing instance of t <typeparamref name="T"/> to populate</param>
        /// <param name="stream">Stream to read data from</param>
        /// <exception caption="" cref="System.IO.EndOfStreamException">If the stream ends before all expected data is deserialized, and the type being deserialized hasn't defined &lt;seealso cref="SDGracefulEofAttribute" /&gt;.</exception>
        public static void Deserialize<T>(ref T obj, SuperStream stream)
            where T : IStreamDataAware
        {
            StreamDataAwareMetaData tm = StreamDataAwareRegistry.Instance.GetInfo(obj.GetType());
                       
            // Parse properties
            foreach (StreamDataPropertyMetaData pMeta in tm.Properties)
            {
                if (stream.EOF)
                {
                    if (tm.GracefulEof)
                    {
                        break;
                    }
                    throw new System.IO.EndOfStreamException(
                        "Unexpectedly met Endof Stream. He was a mean guy.", 
                        new StreamDataException(pMeta));
                }

                if (pMeta.IsCollection)
                {
                    DeserializeHelper.DeserializeCollection(obj, stream, pMeta);
                }
                else
                {
                    DeserializeHelper.DeserializeProperty(obj, stream, pMeta);
                }
            }

            // Run finalizer, if present.
            if (tm.IsFinalizer)
            {
                IStreamDataFinalizer fin = (IStreamDataFinalizer)obj;
                fin.OnDeserialize();
            }
        }
    }
}
