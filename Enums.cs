﻿/*
Savagedlight
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savagedlight.Serialization
{
    public enum StringType
    {
        /// <summary>
        /// A string terminated by a 0-byte (C-style)
        /// </summary>
        CString,
        /// <summary>
        /// A string prefixed by a value indicating its length
        /// </summary>
        Normal
    }

    public enum Endianess
    {
        Little,
        Big
    }

    public enum LengthType
    {
        Byte = -2,
        UInt16 = -1,
        Int32 = 0,
    }

    /// <summary>
    /// Which encoding to use for strings
    /// </summary>
    public enum StringEncoding
    {
        ASCII,
        UTF8,
        Unicode
    }
}
