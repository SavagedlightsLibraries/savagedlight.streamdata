﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace Savagedlight.Serialization
{
    /// <summary>SuperStream is a wrapper class which provides an easy to use API for serializing and deserializing standard value types. It is endian-aware. It supports strings with various
    /// encodings and wrappings, including null-terminated strings.</summary>
    /// <isnew IsNewDateTime="2014-04-27T15:02:15.6003116Z"/>
    /// <example>
    /// 	<code title="Example" description="" lang="C#">
    /// using (MemoryStream ms = new MemoryStream())
    /// {
    ///     using (SuperStream stream = new SuperStream(ms, Endianess.Little)) 
    ///     {
    ///         // Manipulate stream here
    ///     }
    /// }</code>
    /// 	<code title="Converting endianess" description="This example shows how to read big-endian data from one stream and writing it out to another as little-endian data." lang="C#">
    /// public function void ToLittleEndian(MemoryStream src, MemoryStream dst) 
    /// {
    ///     using (SuperStream srcStram = new SuperStream(src, Endianess.Big)) 
    ///     {
    ///         using (SuperStream dstStream = new SuperStream(dst, Endianess.Little))
    ///         {
    ///             // Write an ulong, short, and a null-terminated string
    ///             dstStream.WriteUInt64(srcStream.ReadUInt64());
    ///             dstStream.WriteInt16(srcStream.ReadInt16()); 
    ///             dstStream.WriteCString(srcStream.ReadCString());            
    ///         }
    ///     }
    /// }</code>
    /// </example>
    public class SuperStream : Stream
    {
        /// <summary>This is the default string length data type, when none is specified.</summary>
        public const LengthType StandardStringLengthType = LengthType.Int32;
        public Endianess Endianess { get; private set; }
        public Stream BaseStream { get; private set; }
        private SuperStream alternateEndian { get; set; }
        
        #region Constructors
        /// <summary>
        /// Creates a new instance using the provided stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="endianess">Endianess of data in the stream</param>
        public SuperStream(Stream stream, Endianess endianess)
        {
            this.BaseStream = stream;
            this.Endianess = endianess;
        }
        #endregion

        /// <summary>
        /// Returns this instance if endianess matches, or a wrapper instance with the specified endianess.<br/>
        /// Stream position is synced between streams!
        /// </summary>
        /// <param name="endianess">Endianess of data in the stream</param>
        /// <returns></returns>
        public SuperStream AsEndian(Endianess endianess)
        {
            if (this.Endianess == endianess) { return this; }
            if (this.alternateEndian == null)
            {
                this.alternateEndian = new SuperStream(this.BaseStream, endianess);
            }
            return this.alternateEndian;
        }

        #region BinaryWriter
        /// <summary>
        /// Writes a byte array to stream, as-is.
        /// </summary>
        /// <param name="bytes"></param>
        public void WriteBytes(byte[] bytes)
        {
            this.Write(bytes, 0, bytes.Length);
        }
        #endregion

        #region Write signed integers
        /// <summary>
        /// Writes a 16-bit integer (short) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt16(Int16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }

        /// <summary>
        /// Writes a 32-bit integer (int) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt32(Int32 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }

        /// <summary>
        /// Writes a 64-bit integer (long) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteInt64(Int64 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }
        #endregion

        #region Write unsigned integers
        /// <summary>
        /// Writes a 16-bit unsigned integer (ushort) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt16(UInt16 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }

        /// <summary>
        /// Writes a 32-bit unsigned integer (uint) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt32(UInt32 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }

        /// <summary>
        /// Writes a 64-bit unsigned integer (ulong) to stream, considering endianess.
        /// </summary>
        /// <param name="value"></param>
        public void WriteUInt64(UInt64 value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }
        #endregion

        #region Write floating point numbers
        public void WriteSingle(Single value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }

        public void WriteDouble(Double value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            this.CorrectEndianess(ref bytes);
            this.WriteBytes(bytes);
        }
        #endregion

        /// <summary>
        /// Writes an ASCII string to stream, prefixed with byte length information as described by <paramref name="lengthType"/>
        /// </summary>
        /// <param name="value">String to write to stream.</param>
        /// <param name="lengthType">The data type used to describe the strings length.</param>
        public void WriteString(string value, LengthType lengthType = StandardStringLengthType)
        {
            this.WriteString(value, Encoding.ASCII, lengthType);
        }
        
        /// <summary>
        /// Writes a string using <paramref name="encoding"/>, prefixed with byte length information as described by <paramref name="lengthType"/>.
        /// </summary>
        /// <param name="value">String to write to stream</param>
        /// <param name="encoding">Which encoding to use when converting the string to bytes</param>
        /// <param name="lengthType">The data type used to describe the strings length.</param>
        public void WriteString(string value, Encoding encoding, LengthType lengthType = LengthType.Int32)
        {
            var bytes = encoding.GetBytes(value);
            switch (lengthType)
            {
                case LengthType.Byte:
                    if (byte.MaxValue < bytes.Length)
                    {
                        throw new ArgumentOutOfRangeException("value", "Value is longer than the length type (" + lengthType.ToString() + ") allows.");
                    }
                    this.WriteByte((byte)bytes.Length);
                    break;
                case LengthType.UInt16:
                    if (UInt16.MaxValue < bytes.Length)
                    {
                        throw new ArgumentOutOfRangeException("value", "Value is longer than the length type (" + lengthType.ToString() + ") allows.");
                    }
                    this.WriteUInt16((ushort)bytes.Length);
                    break;
                case LengthType.Int32:
                    // no point checking, as the UInt can always describe the length of an int-indexed array.
                    // - Marie Helene
                    this.WriteUInt32((uint)bytes.Length);
                    break;
                default:
                    throw new NotImplementedException();
            }
            this.WriteBytes(bytes);
        }

        /// <summary>
        /// Writes a null-terminated (C-style) string to stream, using ASCII encoding.
        /// </summary>
        /// <param name="value"></param>
        public void WriteCString(string value)
        {
            this.WriteCString(value, Encoding.ASCII);
        }

        /// <summary>
        /// Writes a null-terminated (C-style) string to stream, using <paramref name="encoding"/>.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="encoding"></param>
        /// <exception cref="ArgumentException">May be thrown if the string contains a null byte</exception>
        public void WriteCString(string value, Encoding encoding)
        {

            var bytes = encoding.GetBytes(value);
            if (bytes.Contains((byte)0))
            {
                // String contains a null byte. 
                // This is not allowed, as the string is terminated by a null byte.
                throw new ArgumentException("Provided string contains a null byte, which is not allowed because the string is terminated by a null byte.", "value");
            }
            this.WriteBytes(bytes);
            this.WriteByte(0);
        }


        #region BinaryReader
        /// <summary>
        /// Read specified amount of bytes, and block thread up to timeout milliseconds.
        /// </summary>
        /// <param name="numBytes">Number of bytes to read</param>
        /// <returns></returns>
        public byte[] ReadBytes(uint numBytes)
        {
            byte[] bytes = new byte[numBytes];
            if (this.EOF)
            {
                throw new EndOfStreamException("EOF reached before reading requested amount of bytes.");
            }
            int readBytes = this.Read(bytes, 0, (int)numBytes);
            if (readBytes != numBytes)
            {
                throw new EndOfStreamException("EOF reached before reading requested amount of bytes.");
            }
            return bytes;
        }

        #region Read Signed Integers
        /// <summary>
        /// Reads a 16-bit integer (short) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public Int16 ReadInt16()
        {
            byte[] bytes = this.ReadBytes(2);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToInt16(bytes, 0);
        }

        /// <summary>
        /// Reads a 32-bit integer (int) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public Int32 ReadInt32()
        {
            byte[] bytes = this.ReadBytes(4);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// Reads a 64-bit integer (long) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public Int64 ReadInt64()
        {
            byte[] bytes = this.ReadBytes(8);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToInt64(bytes, 0);
        }
        #endregion


        #region Read Unsigned Integers
        /// <summary>
        /// Reads a 16-bit unsigned integer (ushort) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public UInt16 ReadUInt16()
        {
            byte[] bytes = this.ReadBytes(2);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToUInt16(bytes, 0);
        }

        /// <summary>
        /// Reads a 32-bit unsigned integer (uint) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public UInt32 ReadUInt32()
        {
            byte[] bytes = this.ReadBytes(4);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToUInt32(bytes, 0);
        }

        /// <summary>
        /// Reads a 64-bit unsigned integer (ulong) from stream, considering endianess.
        /// </summary>
        /// <returns></returns>
        public UInt64 ReadUInt64()
        {
            byte[] bytes = this.ReadBytes(8);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }
        #endregion


        #region Read floating point numbers
        public float ReadSingle()
        {
            byte[] bytes = this.ReadBytes(4);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToSingle(bytes, 0);
        }

        public double ReadDouble()
        {
            byte[] bytes = this.ReadBytes(8);
            this.CorrectEndianess(ref bytes);
            return BitConverter.ToDouble(bytes, 0);
        }
        #endregion

        /// <summary>
        /// Reads an ASCII string from stream, prefixed by length as defined by the standard length type.
        /// </summary>
        /// <returns></returns>
        public string ReadString()
        {
            return this.ReadString(StandardStringLengthType);
        }

        /// <summary>
        /// Reads an ASCII string from stream, prefixed by length as defined by <paramref name="lengthType"/>.
        /// </summary>
        /// <param name="lengthType"></param>
        /// <returns></returns>
        public string ReadString(LengthType lengthType)
        {
            return this.ReadString(lengthType, Encoding.ASCII);
        }

        /// <summary>
        /// Reads a <paramref name="encoding"/>-encoded string from stream, prefixed by length as defined by <paramref name="lengthType"/>
        /// </summary>
        /// <param name="lengthType"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string ReadString(LengthType lengthType, Encoding encoding)
        {
            uint length;
            switch (lengthType)
            {
                case LengthType.Byte:
                    length = (byte)this.ReadByte();
                    break;
                case LengthType.UInt16:
                    length = this.ReadUInt16();
                    break;
                case LengthType.Int32:
                    length = this.ReadUInt32();
                    break;
                default:
                    throw new NotImplementedException();
            }
            return this.ReadString(length, encoding);
        }

        /// <summary>
        /// Reads <paramref name="length"/> bytes, and converts to an <paramref name="encoding"/>-encoded string.
        /// </summary>
        /// <param name="length">Number of bytes to read from stream</param>
        /// <param name="encoding">The strings encoding</param>
        /// <param name="trim">Whether to remove trailing nil-byte</param>
        /// <returns></returns>
        private string ReadString(uint length, Encoding encoding, bool trim=true)
        {
            if (length == 0)
            {
                return String.Empty;
            }
            byte[] bytes = this.ReadBytes(length);
            string str = encoding.GetString(bytes);
            if (trim)
            {
                return str.TrimEnd((char)0);
            }
            else
            {
                return str;
            }
        }

        /// <summary>
        /// Reads a nil-terminated (C-style) ASCII-encoded string.
        /// </summary>
        /// <returns></returns>
        public string ReadCString()
        {
            return this.ReadCString(Encoding.ASCII);
        }


        /// <summary>
        /// Reads a nil-terminated (C-style) string, encoded according to <paramref name="encoding"/>. The trailing nil-byte is removed after reading.
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        /// <exception cref="EndOfStreamException">Thrown if EOF is encountered before a nil byte is read.</exception>
        public string ReadCString(Encoding encoding)
        {
            var sb = new List<byte>();
            bool haveNilByte = false;
            
            while (!this.EOF)
            {
                var b = (byte)this.ReadByte();
                if (b != 0)
                {
                    sb.Add(b);
                }
                else
                {
                    haveNilByte = true;
                    break;
                }
            }

            if (!haveNilByte)
            {
                throw new EndOfStreamException("Reached end of stream before reading a nil byte.");
            }            
            return encoding.GetString(sb.ToArray());
        }


        public bool EOF
        {
            get
            {
                if (this.BaseStream.CanSeek)
                {
                    return this.Position >= this.Length;
                }
                return (!this.BaseStream.CanRead && !this.BaseStream.CanWrite);
            }
        }

        #region Helper methods
        /// <summary>
        /// Reverses the order of <paramref name="bytes"/> if the system and stream endianess are not equal.<br/>
        /// Otherwise, does nothing.
        /// </summary>
        /// <param name="bytes"></param>
        public void CorrectEndianess(ref byte[] bytes)
        {
            bool isLittleEndian = this.Endianess == Endianess.Little;
            
            if (isLittleEndian != BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }
        }
        #endregion
        #endregion



        #region Stream implementation
        public override bool CanRead { get { return this.BaseStream.CanRead; } }

        public override bool CanSeek { get { return this.BaseStream.CanSeek; } }

        public override bool CanWrite { get { return this.BaseStream.CanWrite; } }

        public override void Flush()
        {
            this.BaseStream.Flush();
        }

        public override long Length { get { return this.BaseStream.Length; } }
        public override long Position
        {
            get
            {
                return this.BaseStream.Position;
            }
            set
            {
                this.BaseStream.Position = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return this.BaseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return this.BaseStream.Seek(offset, origin);
        }


        public override void SetLength(long value)
        {
            this.BaseStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.BaseStream.Write(buffer, offset, count);
        }
        #endregion

        
        protected override void Dispose(bool managed)
        {
            base.Dispose(managed);
            if (!managed) { return; }
            this.BaseStream = null;
        }
    }
}
