﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Savagedlight.Serialization;
using Savagedlight;
using Savagedlight.Serialization;

namespace Savagedlight.Serialization.Parsers
{
    public class StringStreamDataParser : IStreamDataParser<string>
    {
        #region IDataParser Members
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out string value)
        {
            StringMetaData meta = new StringMetaData(description.Attributes);
                       
            switch (meta.StringType)
            {
                case StringType.CString:
                    value = stream.ReadCString(meta.Encoding);
                    break;
                case StringType.Normal:
                    value = stream.ReadString(meta.LengthType, meta.Encoding);
                    break;
                default:
                    value = null;
                    throw new Exception("Invalid StringType: " + meta.StringType.ToString());
            }
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, string value)
        {
            StringMetaData meta = new StringMetaData(description.Attributes);
            switch (meta.StringType)
            {
                case StringType.CString:
                    stream.WriteCString((string)value, meta.Encoding);
                    break;
                case StringType.Normal:
                    stream.WriteString(value, meta.Encoding, meta.LengthType);
                    break;
                default:
                    value = null;
                    throw new Exception("Invalid StringType: " + meta.StringType.ToString());
            }
        }
        #endregion

        
        #region Classes
        private class StringMetaData
        {
            public StringType StringType { get; private set; }
            public LengthType LengthType { get; private set; }
            public Encoding Encoding { get; private set; }

            public StringMetaData(Attribute[] attributes)
            {
                this.StringType = this.GetStringType(attributes);
                this.LengthType = this.GetLengthType(attributes);
                this.Encoding = this.GetStringEncoding(attributes);
            }

            private StringType GetStringType(Attribute[] attributes)
            {
                StringType strType = StringType.Normal;
                var attr = attributes.FirstOrDefault(a => a is SDStringAttribute) as SDStringAttribute;
                if (attr != null)
                {
                    strType = attr.Type;
                }
                return strType;
            }

            private LengthType GetLengthType(Attribute[] attributes)
            {
                LengthType lengthType = SuperStream.StandardStringLengthType;
                var attr = attributes.FirstOrDefault(a => a is SDElementLengthAttribute) as SDElementLengthAttribute;
                if (attr != null)
                {
                    lengthType = attr.Type;
                }
                return lengthType;
            }

            private Encoding GetStringEncoding(Attribute[] attributes)
            {
                StringEncoding encoding = StringEncoding.ASCII;
                var attr = attributes.FirstOrDefault(a => a is SDStringAttribute) as SDStringAttribute;
                if (attr != null)
                {
                    encoding = attr.Encoding;
                }

                switch (encoding)
                {
                    default:
                    case StringEncoding.ASCII:
                        return Encoding.ASCII;
                    case StringEncoding.Unicode:
                        return Encoding.Unicode;
                    case StringEncoding.UTF8:
                        return Encoding.UTF8;
                }
            }
        }
        #endregion
    }
}
