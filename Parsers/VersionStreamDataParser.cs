﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Savagedlight.Serialization;

namespace Savagedlight.Serialization.Parsers
{
    /// <summary>
    /// Allows (de)serialization of Version objects.
    /// </summary>
    public class VersionStreamDataParser : IStreamDataParser<Version>
    {
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Version value)
        {
            int a, b, c, d;
            a = stream.ReadInt32();
            b = stream.ReadInt32();
            c = stream.ReadInt32();
            d = stream.ReadInt32();

            // Verify that a and b are valid.
            if (a == -1 || b == -1)
            {
                value = null;
                throw new System.IO.InvalidDataException();
            }

            if (c == -1)
            {
                value = new Version(a, b);
            }
            else if (d == -1)
            {
                value = new Version(a, b, c);
            }
            else
            {
                value = new Version(a, b, c, d);
            }
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Version value)
        {
            var ver = value as Version;
            stream.WriteInt32(ver.Major);
            stream.WriteInt32(ver.Minor);
            stream.WriteInt32(ver.Build);
            stream.WriteInt32(ver.Revision);
        }
    }
}
