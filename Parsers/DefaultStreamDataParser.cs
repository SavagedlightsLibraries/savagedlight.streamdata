﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Savagedlight.Serialization;

namespace Savagedlight.Serialization.Parsers
{
    /// <summary>
    /// Fallback data parser, for handling the most basic types.
    /// </summary>
    public class DefaultStreamDataParser :
        IStreamDataParser<byte>, IStreamDataParser<bool>,
        IStreamDataParser<Int64>, IStreamDataParser<UInt64>,
        IStreamDataParser<Int32>, IStreamDataParser<UInt32>,
        IStreamDataParser<Int16>, IStreamDataParser<UInt16>,
        IStreamDataParser<Single>, IStreamDataParser<Double>
    {
        #region Byte & bool
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out byte value)
        {
            value = (byte)stream.ReadByte();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, byte value)
        {
            stream.WriteByte((byte)value);
        }

        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out bool value)
        {
            var val = stream.ReadUInt32();
            switch (val)
            {
                case 0: 
                    value = false; 
                    break;
                case 1: 
                    value = true; 
                    break;
                default: 
                    throw new System.IO.InvalidDataException("Unexpected data: " + val.ToString() + ". Expected either 0 or 1.");
            }
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, bool value)
        {
            // If bool is true, write 1. Otherwise, write 0.
            int val = value ? 1 : 0;
            stream.WriteInt32(val);
        }
        #endregion


        #region Single & Double
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Single value)
        {
            value = stream.ReadSingle();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Single value)
        {
            stream.WriteSingle(value);
        }

        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Double value)
        {
            value = stream.ReadDouble();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Double value)
        {
            stream.WriteDouble(value);
        }
        #endregion


        #region (U)Int64
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Int64 value)
        {
            value = stream.ReadInt64();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Int64 value)
        {
            stream.WriteInt64(value);
        }

        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out UInt64 value)
        {
            value = stream.ReadUInt64();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, UInt64 value)
        {
            stream.WriteUInt64(value);
        }
        #endregion


        #region (U)Int32
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Int32 value)
        {
            value = stream.ReadInt32();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Int32 value)
        {
            stream.WriteInt32(value);
        }

        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out UInt32 value)
        {
            value = stream.ReadUInt32();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, UInt32 value)
        {
            stream.WriteUInt32(value);
        }
        #endregion


        #region (U)Int16
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out Int16 value)
        {
            value = stream.ReadInt16();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, Int16 value)
        {
            stream.WriteInt16(value);
        }

        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out UInt16 value)
        {
            value = stream.ReadUInt16();
        }

        public void Serialize(SuperStream stream, StreamDataParserInfo description, UInt16 value)
        {
            stream.WriteUInt16(value);
        }
        #endregion

    }
}
