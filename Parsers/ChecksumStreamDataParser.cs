﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Savagedlight.Hash;
using Savagedlight.Serialization;

namespace Savagedlight.Serialization.Parsers
{
    /// <summary>
    /// Parser for MD5, SHA1, SHA256, SHA384 and SHA512 checksums.
    /// </summary>
    public class ChecksumStreamDataParser 
        : IStreamDataParser<MD5Checksum>, IStreamDataParser<SHA1Checksum>,
        IStreamDataParser<SHA256Checksum>, IStreamDataParser<SHA384Checksum>,
        IStreamDataParser<SHA512Checksum>
    {
        
        /// <summary>
        /// Deserializes <see cref="MD5Checksum"/> from <paramref name="stream"/>.
        /// </summary>        
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out MD5Checksum value)
        {
            value = new MD5Checksum(stream.ReadBytes(16));            
        }

        /// <summary>
        /// Deserializes <see cref="SHA1Checksum"/> from <paramref name="stream"/>.
        /// </summary>        
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out SHA1Checksum value)
        {
            value = new SHA1Checksum(stream.ReadBytes(20));            
        }

        /// <summary>
        /// Deserializes <see cref="SHA256Checksum"/> from <paramref name="stream"/>.
        /// </summary>        
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out SHA256Checksum value)
        {
            value = new SHA256Checksum(stream.ReadBytes(32));
        }

        /// <summary>
        /// Deserializes <see cref="SHA384Checksum"/> from <paramref name="stream"/>.
        /// </summary>        
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out SHA384Checksum value)
        {
            value = new SHA384Checksum(stream.ReadBytes(48));            
        }

        /// <summary>
        /// Deserializes <see cref="SHA512Checksum"/> from <paramref name="stream"/>.
        /// </summary>        
        public void Deserialize(SuperStream stream, StreamDataParserInfo description, out SHA512Checksum value)
        {
            value = new SHA512Checksum(stream.ReadBytes(64));
        }

        #region Writers
        private void WriteChecksum(SuperStream ss, Checksum value)
        {
            ss.WriteBytes(value.ToArray());
        }

        /// <summary>
        /// Serializes <paramref name="value"/> into <paramref name="stream"/>.
        /// </summary>        
        public void Serialize(SuperStream stream, StreamDataParserInfo description, MD5Checksum value)
        {
            this.WriteChecksum(stream, value);
        }

        /// <summary>
        /// Serializes <paramref name="value"/> into <paramref name="stream"/>.
        /// </summary>        
        public void Serialize(SuperStream stream, StreamDataParserInfo description, SHA1Checksum value)
        {
            this.WriteChecksum(stream, value);
        }

        /// <summary>
        /// Serializes <paramref name="value"/> into <paramref name="stream"/>.
        /// </summary>        
        public void Serialize(SuperStream stream, StreamDataParserInfo description, SHA256Checksum value)
        {
            this.WriteChecksum(stream, value);
        }

        /// <summary>
        /// Serializes <paramref name="value"/> into <paramref name="stream"/>.
        /// </summary>        
        public void Serialize(SuperStream stream, StreamDataParserInfo description, SHA384Checksum value)
        {
            this.WriteChecksum(stream, value);
        }

        /// <summary>
        /// Serializes <paramref name="value"/> into <paramref name="stream"/>.
        /// </summary>        
        public void Serialize(SuperStream stream, StreamDataParserInfo description, SHA512Checksum value)
        {
            this.WriteChecksum(stream, value);
        }
        #endregion
    }
}
