﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savagedlight.Serialization
{
    /// <summary>
    /// Implements a StreamDataParser for T.
    /// </summary>
    /// <typeparam name="T">The t which will be (de)serialized by this implemnentation</typeparam>
    public interface IStreamDataParser<T>
    {
        /// <summary>
        /// Creates an object (<paramref name="value"/>) from stream (<paramref name="stream"/>)
        /// </summary>
        /// <param name="stream">Stream which will be read from</param>
        /// <param name="description">Description of <paramref name="value"/></param>
        /// <param name="value">Value which will be populated by this method accodding to <paramref name="description"/></param>
        void Deserialize(SuperStream stream, StreamDataParserInfo description, out T value);

        /// <summary>
        /// Takes an object <paramref name="value"/> and writes it to <paramref name="stream"/> according to <paramref name="description"/>
        /// </summary>
        /// <param name="stream">Stream which will be written to</param>
        /// <param name="description">Description of <paramref name="value"/></param>
        /// <param name="value">Value which will be serialized to <paramref name="stream"/> accodrding to <paramref name="description"/></param>
        void Serialize(SuperStream stream, StreamDataParserInfo description, T value);
    }
}
